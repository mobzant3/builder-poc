/* eslint-disable @next/next/no-sync-scripts */
const FontAwesomeScript = () => (
  <script
    src="https://kit.fontawesome.com/35e7d06203.js"
    crossOrigin="anonymous"
  />
);

export default FontAwesomeScript;
