/** @jsxImportSource theme-ui */

import { Box, Grid } from "theme-ui";
import Card from "./Card";

type ZigZagBuilderIoPayload = {
  card: {
    subtitle: string;
    imageUrl: string;
    title: string;
  };
};

type ZigZagNBoProps = {
  cards: ZigZagBuilderIoPayload[];
};

const ZigZagNB = (builderProps: ZigZagNBoProps) => {
  return (
    <Box sx={{ backgroundColor: "gray2" }}>
      <Grid sx={{ paddingBottom: "130px" }}>
        {builderProps.cards?.map((data, i) => {
          if (!data || Object.keys(data).length === 0) {
            return <></>;
          }

          const { subtitle, title, imageUrl } = data.card;

          return (
            <Card
              key={title}
              title={title}
              subtitle={subtitle}
              imageUrl={imageUrl}
              toTheRight={i % 2 !== 0}
            />
          );
        })}
      </Grid>
    </Box>
  );
};

export default ZigZagNB;
