/** @jsxImportSource theme-ui */
import { Grid, Heading, Text } from "theme-ui";
import Image from "next/image";

type CardProps = {
  title: string;
  subtitle: string;
  imageUrl: string;
  toTheRight: boolean;
};

const Card = ({ title, subtitle, imageUrl, toTheRight }: CardProps) => {
  return (
    <Grid
      id={undefined}
      columns={[1, null, "repeat(2, fit-content(50%))"]}
      gap={[null, null, 5, "85px"]}
      sx={{
        "&:nth-of-type(4)": {
          scrollMarginTop: "0",
        },
        scrollMarginTop: "8rem",
        px: ["30px", "100px", null, "192px"],
        gridAutoFlow: "dense",
        textAlign: ["center", null, "left"],
        alignItems: "center",
        justifyContent: "center",
        background: "#f9f9f9",
        backgroundRepeat: "no-repeat",
        backgroundSize: ["auto", null, "100% 100%"],
        backgroundPosition: "center",
        py: ["35px", "35px", null],
        paddingBottom: "30px !important",
        "@media screen and (max-width: 425px)": {
          px: "25px",
        },
        ":last-of-type": {
          mb: "15px",
        },
      }}
    >
      <div
        sx={{
          gridColumn: toTheRight ? ["1", null, "2"] : "1",
          color: ["gray10"],
        }}
      >
        <Heading
          as="h1"
          variant="heading.3"
          sx={{
            mb: 3,
            "@media screen and (max-width: 831px)": {
              mt: "",
            },
            fontWeight: "600",
            maxWidth: ["100%", "100%", "450px"],
            letterSpacing: "0px",
          }}
        >
          {title}
        </Heading>
        <Text
          variant="normal"
          sx={{
            mb: 3,
            maxWidth: ["100%", "100%", "520px"],
            lineHeight: "24px",
          }}
        >
          {subtitle}
        </Text>
      </div>
      <div
        sx={{
          maxWidth: "100%",
          height: ["fit-content", null, "initial"],
          width: ["100%", null],
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          position: "relative",
          "& > div": {
            "@media screen and (min-width: 1600px)": { width: "574.5px" },
          },
        }}
      >
        <div
          sx={{
            maxWidth: "100%",
            width: ["100%", null],
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            position: "relative",
          }}
        >
          {imageUrl && (
            <Image
              sx={{
                width: ["100%"],
                height: ["100%"],
                objectFit: "fill",
              }}
              src={imageUrl}
              alt={"Zig-zag-image"}
              width={720}
              height={480}
              quality={100}
            />
          )}
        </div>
      </div>
    </Grid>
  );
};

export default Card;
