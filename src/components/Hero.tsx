/** @jsxImportSource theme-ui */
import { Box, Container, Grid, Heading, Link, Text } from "theme-ui";
import Image from "next/image";

type ButtonBuilderIoPayload = {
  button: {
    label: string;
    href: string;
  };
};

type HeroProps = {
  title: string;
  subtitle: string;
  imageUrl: string;
  buttons: ButtonBuilderIoPayload[];
};

const Hero = ({ title, subtitle, imageUrl, buttons = [] }: HeroProps) => {
  return (
    <>
      <Grid
        gap={1}
        columns={1}
        sx={{
          maxWidth: "none",
          margin: "0",
          height: [500, 500, 500, 500, 500, 530],
          backgroundColor: "#22184D",
          position: "relative",
          "&>div:nth-of-type(1)": {
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-start",
            "@media screen and (min-width: 1800px)": {
              alignItems: "flex-end",
            },
          },
          "&>div:nth-of-type(1)>div>div>div:nth-of-type(1)": {
            maxWidth: ["none", "none", "600px", "600px", "600px", "600px"],
            mt: ["24px", "24px", "50px", "60px", "78px", "78px"],
            ml: [0, 0, 35, 60, 100, 180],
            color: "background",
            "@media screen and (max-width: 1500px) and (min-width: 1442px)": {
              ml: 70,
            },
            "@media screen and (max-width: 1350px) and (min-width: 1280px)": {
              ml: 40,
            },
            "@media screen and (max-width: 960px) and (min-width: 832px)": {
              ml: 0,
              mt: 80,
            },
            "@media screen and (max-width: 425px)": {
              mt: "-15px",
            },
            "@media screen and (max-width: 380px)": {
              mt: "10px",
            },
            "@media screen and (max-width: 320px)": {
              mt: "-10px",
            },
          },
          "&>div:nth-of-type(1)>div>div>div:nth-of-type(2)": {
            mt: ["15px", "15px", "0", "10px", "15px", "10px"],
            ml: [0, 0, 35, 60, 100, 180],
            display: "flex",
            width: "100%",
            justifyContent: [
              "center",
              "center",
              "flex-start",
              "flex-start",
              "flex-start",
              "flex-start",
            ],
            "@media screen and (max-width: 380px)": {
              mt: "0",
            },
            "@media screen and (max-width: 1500px) and (min-width: 1442px)": {
              ml: 70,
            },
            "@media screen and (max-width: 1350px) and (min-width: 1280px)": {
              ml: 40,
            },
            "@media screen and (max-width: 960px) and (min-width: 832px)": {
              ml: 0,
            },
          },
          "&>div:nth-of-type(1)>div>div>div:nth-of-type(1)>h1": {
            textAlign: ["center", "center", "left", "left", "left", "left"],
            fontSize: ["55px", "55px", "55px", "60px", "65px", "65px"],
            lineHeight: "67px",
            letterSpacing: "0px",
            fontWeight: 600,
            "@media screen and (max-width: 960px) and (min-width: 832px)": {
              fontSize: "50px",
              lineHeight: "50px",
            },
            "@media screen and (max-width: 565px)": {
              fontSize: "45px",
              lineHeight: "50px",
            },
            "@media screen and (max-width: 380px)": {
              fontSize: "34px",
              lineHeight: "40px",
            },
          },
          "&>div:nth-of-type(1)>div>div>div:nth-of-type(1)>div>p": {
            textAlign: ["center", "center", "left", "left", "left", "left"],
            fontSize: ["19px", "19px", "19px", "21px", "22px", "22px"],
            mt: ["28px", "28px", "-5px", "10px", "20px", "28px"],
            ml: "0",
            fontWeight: 400,
            letterSpacing: "0px",
            lineHeight: "26px",
            maxWidth: ["none", "none", "490px", "540px", "570px", "570px"],
            "@media screen and (max-width: 960px) and (min-width: 832px)": {
              fontSize: "17px",
              lineHeight: "24px",
              maxWidth: "430px",
            },
            "@media screen and (max-width: 380px)": {
              mt: "15px",
            },
          },
        }}
      >
        <Box>
          <Box
            sx={{
              "& div": {
                height: [
                  "630px",
                  "630px",
                  "630px",
                  "630px",
                  "630px",
                  "630px",
                  "630px",
                ],
                "@media screen and (min-width: 394px) and (max-width: 449px)": {
                  height: "630px",
                },
                "@media screen and (min-width: 350px) and (max-width: 600px)": {
                  height: "630px",
                },
                "@media screen and (min-width: 808px) and (max-width: 831px)": {
                  height: "630px",
                },
                "@media screen and (min-width: 1025px) and (max-width: 1280px)":
                  {
                    height: "630px",
                  },
                "@media screen and (min-width: 200px) and (max-width: 350px)": {
                  height: "630px",
                },
                "& > img": {
                  objectFit: "fill !important" as any,
                  "@media screen and (min-width: 200px) and (max-width: 702px)":
                    {
                      objectFit: "cover !important" as any,
                    },
                  "@media screen and (min-width: 1500px) and (max-width: 5000px)":
                    {
                      objectFit: "fill !important" as any,
                    },
                },
              },
            }}
          >
            <Image
              sx={{
                minWidth: [
                  "166% !important",
                  "166% !important",
                  "128% !important",
                  "100%",
                  "100%",
                ],
                left: ["-200px !important", "-200px !important", 0, 0, 0],
                position: "absolute",
                width: ["100%"],
                height: ["100%"],
                objectFit: "fill",
                objectPosition: "bottom",
              }}
              unoptimized
              src={imageUrl}
              width="0"
              height="0"
              quality={100}
              priority
              alt="home-img"
              draggable={false}
              loading="eager"
            />
          </Box>
          <Container
            variant={"section"}
            sx={{
              color: "gray10",
              pb: 0,
              display: "flex",
              justifyContent: "center",
              flexDirection: "column",
              alignItems: "center",
              "& h1": {
                textAlign: "center !important" as any,
              },
              "& p": {
                marginTop: "10px !important",
                marginBottom: "15px",
                minWidth: [200, 500, 600, 943, 943, 943],
                textAlign: "center !important" as any,
              },
              "& > div:first-of-type": {
                pt: "0px",
                width: "100% !important",
                margin: "0px !important",
                display: "flex",
                justifyContent: "center",
                flexDirection: "column",
              },
              "& > div > div": {
                margin: "0px !important",
                alignSelf: "center !important",
                display: "flex",
              },
              "& > div + div": {
                mx: "0px !important",
                justifyContent: "center !important",
              },
            }}
          >
            <Box
              sx={{
                zIndex: "general",
                position: "relative",
                mb: "24px",
              }}
            >
              <Heading
                as={"h1"}
                sx={{
                  position: "relative",
                  color: "#f1f8ff",
                  mb: "16px",
                  fontWeight: 500,
                  mx: "auto",
                  lineHeight: "65px",
                  textAlign: "center",
                  fontSize: "65px",
                  letterSpacing: "0px",
                }}
              >
                <span
                  sx={{ position: "absolute", top: "-164px" }}
                  id={"title"}
                />
                {title}
              </Heading>
              <Text
                sx={{
                  fontSize: "22px",
                  color: "#f1f8ff",
                  lineHeight: "26px",
                  fontWeight: 300,
                  textAlign: "center",
                  mx: "auto",
                  maxWidth: "950px",
                  "p:not(:last-of-type)": { mb: 3 },
                }}
              >
                <Text as="p" sx={{ ":not(:last-of-type)": { mb: "8px" } }}>
                  {subtitle}
                </Text>
              </Text>
            </Box>
            <Box
              style={{ textAlign: "center", width: "fit-content !important" }}
            >
              {buttons.map((data, index) => {
                if (!data.button) {
                  return <></>;
                }

                const { button } = data;

                return (
                  <Link
                    key={button.label}
                    variant={
                      index === 0
                        ? "buttons.secondary"
                        : "buttons.darkSecondary"
                    }
                    href={button.href}
                    sx={{
                      ":not(:last-of-type)": { mr: 15 },
                      height: "35px",
                      textTransform: "uppercase",
                      fontSize: ["11px", "12px"],
                      letterSpacing: "0.5px",
                      lineHeight: "16px",
                      ml: "2px",
                      px: ["13px", "33px"],
                      ":last-of-type": {
                        px: "23.5px",
                        lineHeight: "17px",
                      },
                      width: "fit-content",
                    }}
                  >
                    {button.label}
                  </Link>
                );
              })}
            </Box>
          </Container>
        </Box>
      </Grid>
    </>
  );
};

export default Hero;
