/** @jsxImportSource theme-ui */
import { Heading, Text } from "theme-ui";

type CardProps = {
  title: string;
  subtitle: string;
};

const Card = ({ title, subtitle }: CardProps) => {
  return (
    <div
      sx={{
        color: "gray10",
        backgroundColor: "background",
        display: "flex",
        flexDirection: "column",
        padding: ["15px", "29px", "29px", "29px", "29px", "29px"],
        borderRadius: "4px",
        width: "274px",
        textAlign: "center",
        flex: 1,
        "@media screen and (max-width: 1163px)": {
          boxShadow: "card",
          width: "100%",
          height: "100%",
        },
      }}
    >
      <Heading
        as="h1"
        variant={"heading.base"}
        sx={{
          fontWeight: "600",
          fontSize: ["28px", "30px", "38px", "38px", "48px", "48px"],
          mt: 0,
          mb: "10px",
          color: "#6742F1",
        }}
      >
        {title}
      </Heading>
      <Text
        variant="normal"
        sx={{
          lineHeight: "20px",
          mb: "13px",
          "&>p": { mb: "8px" },
        }}
      >
        {subtitle}
      </Text>
    </div>
  );
};

export default Card;
