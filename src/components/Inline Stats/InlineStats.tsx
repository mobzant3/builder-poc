/** @jsxImportSource theme-ui */
import { Box, Grid } from "theme-ui";
import { Fragment } from "react";

import Card from "./Card";

type InlineStatsBuilderIoPayload = {
  card: {
    subtitle: string;
    title: string;
  };
};

type InlineStatsNBoProps = {
  cards: InlineStatsBuilderIoPayload[];
};

const InlineStats = ({ cards }: InlineStatsNBoProps) => (
  <Box
    sx={{
      display: "flex",
      justifyContent: "center",
      minHeight: 200,
      px: ["32px", "100px", "100px", "192px", "192px"],
      background: "#f9f9f9",
    }}
  >
    <Box
      sx={{
        display: "flex",
        justifyContent: "space-between",
        boxShadow: "card",
        maxWidth: "1172px",
        maxHeight: "170px",
        width: "100%",
        borderRadius: "4px",
        background: "white",
        "@media screen and (max-width: 1162px)": {
          display: "none",
        },
      }}
    >
      {cards?.map((data, i) => {
        if (!data.card) {
          return <></>;
        }

        const { subtitle, title } = data.card;

        return (
          <Fragment key={`${title}_${i}`}>
            <Card title={title} subtitle={subtitle} />
            {i !== cards.length - 1 && (
              <div
                style={{
                  width: "2px",
                  background: "#EEEEEE",
                  marginBottom: "29px",
                  marginTop: "29px",
                }}
              ></div>
            )}
          </Fragment>
        );
      })}
    </Box>
    <Grid
      columns={[
        "1fr 1fr",
        "1fr 1fr",
        "1fr 1fr",
        "1fr 1fr",
        "1fr 1fr",
        "1fr 1fr",
        "1fr 1fr",
      ]}
      sx={{
        maxWidth: "1172px",
        width: "100%",
        gap: "20px",
        "@media screen and (min-width: 1163px)": {
          display: "none",
        },
        "@media screen and (max-width: 400px)": {
          gridTemplateColumns: "1fr !important",
        },
      }}
    >
      {cards?.map((data, i) => {
        if (!data.card) {
          return <></>;
        }

        const { subtitle, title } = data.card;

        return (
          <div key={title} sx={{ boxShadow: "card" }}>
            <Card key={title} title={title} subtitle={subtitle} />
          </div>
        );
      })}
    </Grid>
  </Box>
);

export default InlineStats;
