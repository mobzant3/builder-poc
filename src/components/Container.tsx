/** @jsxImportSource theme-ui */
import { BuilderBlocks, BuilderElement } from "@builder.io/react";
import { Box } from "theme-ui";
import Image from "next/image";

const defaultBackgroundColor = "#f9f9f9";

type ContainerProps = {
  backgroundColor?: string;
  children: any;
  maxWidth: number;
  height: number;
  imageUrl: string;
  builderBlock: BuilderElement;
};

function Container({
  backgroundColor = defaultBackgroundColor,
  maxWidth,
  height,
  builderBlock,
  imageUrl,
}: ContainerProps) {
  return (
    <Box
      as="main"
      sx={{
        position: "relative",
        background: backgroundColor,
        width: "100%",
        height
      }}
    >
      {imageUrl && (
        <Image
          sx={{
            position: "absolute",
            width: ["100%"],
            height: ["100%"],
            objectFit: "fill",
          }}
          unoptimized
          src={imageUrl}
          width="0"
          height="0"
          quality={100}
          priority
          alt="home-img"
          draggable={false}
          loading="eager"
        />
      )}

      <section
        sx={{
          maxWidth: maxWidth ? `${maxWidth}px` : "initial",
          margin: "auto",
        }}
      >
        <BuilderBlocks
          key={"main-content"}
          blocks={builderBlock.children}
          parentElementId={builderBlock.id}
          dataPath="this.children"
        />
      </section>
    </Box>
  );
}

export default Container;
