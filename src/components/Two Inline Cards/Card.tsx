/** @jsxImportSource theme-ui */
import { Heading, Text } from "theme-ui";
import getIcon from "../../lib/get-icon";

type CardProps = {
  title: string;
  subtitle: string;
  actions: any;
  icon: string;
  isLarge: boolean;
  additionalTitle: string;
};

const Card = ({
  title,
  subtitle,
  actions,
  icon,
  isLarge,
  additionalTitle,
}: CardProps) => {
  const Icon = getIcon(icon);
  const fontAwesomeType = "fa-thin";
  return (
    <div
      sx={{
        color: "gray10",
        backgroundColor: "background",
        width: "initial",
        maxWidth: !isLarge ? "none" : "473px",
        display: "flex",
        borderRadius: "4px",
        boxShadow: !isLarge ? "card" : "none",
        padding: !isLarge ? "30px" : "none",
        textAlign: !isLarge ? "left" : "left",
      }}
    >
      <div
        sx={{
          paddingTop: "5px",
          borderRadius: "full",
          variant: "none",
          mt: "0px",
          color: "iconPrimary",
          fontSize: 4,
          display: "flex",
          alignItems: "flex-start",
          marginRight: "18px",
          justifyContent: ["center", "center", "start"],
        }}
      >
        {Icon && (
          <i
            className={`${fontAwesomeType} ${icon} `}
            sx={{
              fontSize: "45px",
              display: "inline-flex",
              justifyContent: "center",
              alignItems: "start",
            }}
          ></i>
        )}
      </div>
      <div>
        <Heading
          as="p"
          sx={{
            color: "#6742F1",
            fontWeight: "700",
            mt: 0,
            mb: "4px",
          }}
        >
          {additionalTitle}
        </Heading>
        <Heading
          as="h1"
          variant={"heading.3"}
          sx={{
            fontWeight: "600",
            mt: 0,
            mb: "13px",
          }}
        >
          {title}
        </Heading>
        <Text
          variant={"normal"}
          sx={{
            lineHeight: "20px",
            maxWidth: "413px",
            mb: "13px",
            "&>p": { mb: "8px" },
          }}
        >
          {subtitle}
        </Text>
        {/* {actions?.items[0] && (
          <Link
            key={value?.data?.label}
            variant={
              index === 0 ? "buttons.secondary" : "buttons.darkSecondary"
            }
            href={value?.data.href}
            sx={{
              ":not(:last-of-type)": { mr: 15 },
              height: "35px",
              textTransform: "uppercase",
              fontSize: ["11px", "12px"],
              letterSpacing: "0.5px",
              lineHeight: "16px",
              ml: "2px",
              px: ["13px", "33px"],
              ":last-of-type": {
                px: "23.5px",
                lineHeight: "17px",
              },
              width: "fit-content",
            }}
          >
            {value?.data.label}
          </Link>
        )} */}
      </div>
    </div>
  );
};

export default Card;
