/** @jsxImportSource theme-ui */
import { Box, Grid } from "theme-ui";
import Card from "./Card";
import { Fragment } from "react";

type CardBuilderIoPayload = {
  card: {
    title: string;
    subtitle: string;
    icon: string;
    additionalTitle: string;
    action: {
      label: string;
      href: string;
    };
  };
};

type TwoInlineCardsProps = {
  cards: CardBuilderIoPayload[];
};

const TwoInlineCards = ({ cards }: TwoInlineCardsProps) => {
  return (
    <Box
      sx={{
        minHeight: "223px",
        display: "flex",
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        textAlign: "center",
        px: ["32px", "100px", "100px", "192px", "192px"],
        background: "#f9f9f9",
        paddingBottom: ["50px", "40px", "40px", "40px", 0, 0],
        marginTop: ["-26px", "-26px", "-26px", "-26px", 0, 0],
      }}
    >
      <Box
        sx={{
          maxWidth: "1172px",
          display: "flex",
          textAlign: ["center", null, "left"],
          boxShadow: "card",
          width: "100%",
          borderRadius: "4px",
          justifyContent: [
            "space-between",
            "space-between",
            "space-between",
            "space-between",
            "space-between",
            "start",
          ],
          padding: "30px",
          background: "white",
          zIndex: 0,
          marginTop: "-150px",
          "@media screen and (max-width: 1162px)": {
            display: "none",
          },
        }}
      >
        {cards?.map((data, i) => {
          const dataKeys = Object.keys(data);
          if (dataKeys.length === 0) {
            return <></>;
          }
          if (data.card) {
            const { title, subtitle, icon, additionalTitle, action } =
              data.card;
            return (
              <Fragment key={`${title}_${i}_mobile`}>
                <Card
                  key={title}
                  title={title}
                  subtitle={subtitle}
                  actions={[action]}
                  icon={icon}
                  isLarge
                  additionalTitle={additionalTitle}
                />
                {i === 0 && (
                  <div
                    sx={{
                      marginLeft: [
                        "40px",
                        "40px",
                        "40px",
                        "40px",
                        "40px",
                        "83.3125px",
                      ],
                      marginRight: [
                        "40px",
                        "40px",
                        "40px",
                        "40px",
                        "40px",
                        "40px",
                      ],
                      width: "2px",
                      background: "#EEEEEE",
                    }}
                  ></div>
                )}
              </Fragment>
            );
          }
          return <></>;
        })}
      </Box>
      <Grid
        columns={["1fr", "1fr", "repeat(2, 1fr)"]}
        sx={{
          zIndex: 9,
          maxWidth: "1172px",
          width: "100%",
          gap: "20px",
          "@media screen and (min-width: 1163px)": {
            display: "none",
          },
        }}
      >
        {cards?.map((data) => {
          const dataKeys = Object.keys(data);
          if (dataKeys.length === 0) {
            return <></>;
          }

          if (data.card) {
            const { title, subtitle, icon, additionalTitle, action } =
              data.card;
            return (
              <Card
                key={title}
                title={title}
                subtitle={subtitle}
                actions={[action]}
                icon={icon}
                isLarge={false}
                additionalTitle={additionalTitle}
              />
            );
          }
          return <></>;
        })}
      </Grid>
    </Box>
  );
};

export default TwoInlineCards;
