/** @jsxImportSource theme-ui */

import { Heading } from "theme-ui";

type TextProps = {
  text: string;
  color: string;
  asElement: "h1" | "h2" | "h3" | "span";
  variant?: "heading.1" | "heading.2" | "heading.3" | "span";
  pushSx?: any;
};

const Text = ({
  text,
  variant,
  asElement = "span",
  color = "black",
  pushSx,
}: TextProps) => {
  return (
    <Heading
      as={asElement}
      variant={variant}
      sx={{
        color,
        position: "relative",
        mb: 0,
        mx: "auto",
        textAlign: "center",
        letterSpacing: "0px",
        ...pushSx
      }}
    >
      {text}
    </Heading>
  );
};

export default Text;
