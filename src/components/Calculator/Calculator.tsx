/** @jsxImportSource theme-ui */

import {
  Dispatch,
  JSXElementConstructor,
  Key,
  ReactElement,
  ReactFragment,
  useCallback,
  useMemo,
  useState,
} from "react";
import { Box, Container, Flex, jsx, Select, Text } from "theme-ui";
import { formatPrice } from "../../lib/util";
import BackupGbItem from "./BackupGbItem";
import CalculatorItem from "./item";

const buttonWidth = "72px";
const aMillion = 1000000;

const calculatorItems = [
  {
    label: "Monthly read operations",
    id: "readOperationsMillion",
    placeholder: "",
    calculate: (v: number, ratePerMillion: number) => {
      const ratePerOperation = ratePerMillion / aMillion;
      return v * ratePerOperation;
    },
  },
  {
    label: "Monthly write operations",
    id: "writeOperationsMillion",
    placeholder: "",
    calculate: (v: number, ratePerMillion: number) => {
      const ratePerOperation = ratePerMillion / aMillion;
      return v * ratePerOperation;
    },
  },
  {
    label: "Monthly compute operations",
    id: "transactionalComputeOperationsMillion",
    placeholder: "",
    calculate: (v: number, ratePerMillion: number) => {
      const ratePerOperation = ratePerMillion / aMillion;
      return v * ratePerOperation;
    },
  },
  {
    label: "Monthly data stored (GB)",
    id: "storageGb",
    placeholder: "",
    calculate: (v: number, ratePerGB: number) => {
      return v * ratePerGB;
    },
  },
] as any;

type PriceCalculatorSectionProps = {
  title: any;
  subtitle: any;
  blocks: any;
  totalLabel: any;
  anchorLinkId: any;
  isPricingPage: any;
};

const blocksMock = [
  {
    name: "Individual",
    description: "For professional developers building production applications",
    priceLabel: "from **$25/month**",
    creditsPerMonth: 25,
    listItems: [
      {
        title: "Capabilities",
      },
      {
        title: "Quota (per month)",
      },
      {
        title: "Support",
      },
    ],
    usageRate: {
      id: "standard",
      label: "standard",
      readOperations: 0.5,
      writeOperations: 2.5,
      transactionalComputeOperations: 0.5,
      storage: 0.25,
    },
    usageRatesCollection: [
      {
        id: "standard",
        label: "standard",
        readOperations: 0.5,
        writeOperations: 2.5,
        transactionalComputeOperations: 0.5,
        storage: 0.25,
      },
    ],
  },
  {
    name: "Team",
    description: "For teams of developers building rich applications",
    priceLabel: "from **$150/month**",
    creditsPerMonth: 150,
    listItems: [
      {
        title: "Capabilities",
      },
      {
        title: "Quota (per month)",
      },
      {
        title: "Support",
      },
    ],
    usageRate: {
      id: "standard",
      label: "standard",
      readOperations: 0.5,
      writeOperations: 2.5,
      transactionalComputeOperations: 0.5,
      storage: 0.25,
    },
    usageRatesCollection: [
      {
        id: "standard",
        label: "standard",
        readOperations: 0.5,
        writeOperations: 2.5,
        transactionalComputeOperations: 0.5,
        storage: 0.25,
      },
    ],
  },
  {
    name: "Business",
    description:
      "For enterprise teams building full-featured, global applications",
    priceLabel: "from **$500/month**",
    creditsPerMonth: 500,
    listItems: [
      {
        title: "Capabilities",
      },
      {
        title: "Quota (per month)",
      },
      {
        title: "Support",
      },
    ],
    usageRate: {
      id: "standard",
      label: "standard",
      readOperations: 0.5,
      writeOperations: 2.5,
      transactionalComputeOperations: 0.5,
      storage: 0.25,
    },
    usageRatesCollection: [
      {
        id: "standard",
        label: "standard",
        readOperations: 0.5,
        writeOperations: 2.5,
        transactionalComputeOperations: 0.5,
        storage: 0.25,
      },
    ],
  },
];

const PriceCalculatorSection = ({
  blocks = [],
  totalLabel = "Total",
  isPricingPage = true,
}: PriceCalculatorSectionProps) => {
  const [selectedPlan, setSelectedPlan] = useState<string>(
    blocks.length > 0 ? blocks[0].name : ""
  );
  const [selectedRegionGroup, setSelectedRegionGroup] = useState<string>(() => {
    const firstBlock =
      blocks.length > 0
        ? blocks[0]
        : { usageRatesCollection: [{ label: "test" }] };
    return firstBlock.usageRatesCollection
      ? firstBlock[0]?.label
      : { usageRate: 1, usageRatesCollection: [{ u: { label: "test" } }] };
  });

  const [values, setValues] = useState({
    readOperationsMillion: 0,
    storageGb: 0,
    transactionalComputeOperationsMillion: 0,
    writeOperationsMillion: 0,
  } as any);

  const [backupGBvalue, setBackupGBvalue] = useState(0);

  const plan =
    blocks.length > 0
      ? blocks?.find((p: { name: string }) => p.name === selectedPlan) || {
          usageRate: 1,
          usageRatesCollection: [{ u: { label: "test" } }],
        }
      : { usageRate: 1, usageRatesCollection: [{ u: { label: "test" } }] };
  const usageRate = useMemo(() => {
    console.log(plan);
    plan.usageRatesCollection = plan.usageRatesCollection
      ? plan.usageRatesCollection
      : [{ u: { label: "test" } }];
    return plan?.usageRatesCollection.find(
      (u: { label: string }) => u?.label === selectedRegionGroup
    ) ?? plan
      ? plan.usageRate
      : "10";
  }, [plan, selectedRegionGroup]);

  const total = calculateTotal(values, backupGBvalue, plan?.creditsPerMonth);

  const handleSelectedPlanChange = useCallback(
    (e: React.ChangeEvent<HTMLSelectElement>) => {
      setSelectedPlan(e.target.value);
    },
    []
  );

  const handleSelectedRegionGroupChange = useCallback(
    (e: React.ChangeEvent<HTMLSelectElement>) => {
      setSelectedRegionGroup(e.target.value);
    },
    []
  );

  const sectionLayoutExtraProps = isPricingPage
    ? { backgroundColor: "#F9F9F9" }
    : {};

  const pushSx = isPricingPage
    ? {
        "@media screen and (min-width: 1100px)": {
          backgroundColor: "gray2",
          maxWidth: "1200px",
          display: "flex",
          px: 0,
          margin: "auto",
          alignItems: "center",
          "&>div:nth-of-type(1)": {
            width: 473,
            marginRight: "auto",
            "&>h2:nth-of-type(1)": {
              textAlign: "start",
            },
            "&>div:nth-of-type(1)": {
              textAlign: "start",
            },
          },
          "&>div:nth-of-type(2)": {
            display: "none",
          },
          "&>div:nth-of-type(3)": {
            margin: "0px",
          },
        },
      }
    : { backgroundColor: "gray2" };

  return (
    <Box
      sx={{
        position: "relative",
        zIndex: "general",
        width: "100% !important",
        backgroundColor: "#F9F9F9",
      }}
      id={undefined}
    >
      <Container
        variant={"section"}
        sx={
          {
            color: "gray10",
            pt: ["16px", "32px"],
            py: "",
            pb: "",
            ...pushSx,
          } as any
        }
      >
        <div sx={{ mx: "auto", maxWidth: isPricingPage ? "605px" : "unset" }}>
          <Box
            sx={{
              maxWidth: "605px",
              minHeight: "436px",
              mx: "auto",
              backgroundColor: "background",
              p: "23px",
              boxShadow: "card",
              borderRadius: "8px",
            }}
          >
            <Flex sx={{ flexDirection: ["column", "row"] }}>
              <Flex
                sx={{
                  justifyContent: ["space-between", "flex-start"],
                  flexDirection: ["row", "column"],
                }}
              >
                <label
                  htmlFor="selector"
                  sx={{
                    pr: [2, 3],
                    color: "gray8",
                    fontSize: "14px",
                    fontWeight: "600",
                  }}
                >
                  Select plan
                </label>
                <div>
                  <Select
                    id="selector"
                    onChange={handleSelectedPlanChange}
                    value={selectedPlan}
                    sx={{
                      fontSize: "14px",
                      width: "170px",
                      height: "36px",
                      border: "1px solid #797E80",
                      borderRadius: "4px",
                      py: 0,
                      pl: 1,
                    }}
                  >
                    {blocks?.map(
                      (plan: {
                        name:
                          | boolean
                          | Key
                          | ReactElement<
                              any,
                              string | JSXElementConstructor<any>
                            >
                          | ReactFragment
                          | null
                          | undefined;
                      }) => (
                        <option
                          key={plan.name as any}
                          value={plan.name as string}
                        >
                          {plan.name}
                        </option>
                      )
                    )}
                  </Select>
                </div>
              </Flex>

              <Flex
                sx={{
                  justifyContent: ["space-between", "flex-start"],
                  flexDirection: ["row", "column"],
                  ml: [0, 3],
                  mt: [2, 0],
                }}
              >
                <label
                  htmlFor="region-group"
                  sx={{
                    pr: [2, 3],
                    color: "gray8",
                    fontSize: "14px",
                    fontWeight: "600",
                  }}
                >
                  Select region group
                </label>
                <div>
                  <Select
                    id="region-group"
                    onChange={handleSelectedRegionGroupChange}
                    value={selectedRegionGroup}
                    sx={{
                      fontSize: "14px",
                      width: "170px",
                      height: "36px",
                      border: "1px solid #797E80",
                      borderRadius: "4px",
                      py: 0,
                      pl: 1,
                    }}
                  >
                    {plan?.usageRatesCollection?.map(
                      (
                        usageRate: {
                          label:
                            | string
                            | number
                            | boolean
                            | ReactElement<
                                any,
                                string | JSXElementConstructor<any>
                              >
                            | ReactFragment
                            | null
                            | undefined;
                        },
                        i: Key | null | undefined
                      ) => (
                        <option key={i} value={usageRate?.label as string}>
                          {usageRate?.label}
                        </option>
                      )
                    )}
                  </Select>
                </div>
              </Flex>
            </Flex>
            <div
              sx={{
                "> div:not(:last-of-type)": { mb: "15px" },
                mt: "15px",
                mb: "15px",
              }}
            >
              {calculatorItems.map(
                (
                  item: jsx.JSX.IntrinsicAttributes & {
                    label: string;
                    id: any;
                    placeholder: string;
                    usageRate: any;
                    calculate: (v: number, rate: number) => number;
                    value: number;
                    setValues: Dispatch<any>;
                  }
                ) => (
                  <CalculatorItem
                    key={item.id}
                    {...item}
                    value={values[item.id as string] as any}
                    setValues={setValues}
                    usageRate={usageRate}
                    placeholder="0"
                  />
                )
              )}
              <BackupGbItem
                setBackupGbValue={setBackupGBvalue}
                backupGbValue={backupGBvalue}
              />
            </div>
            <Flex
              sx={{
                justifyContent: "right",
                alignItems: "center",
                flexDirection: ["column-reverse", "row"],
              }}
            >
              <Text variant="small" sx={{ mr: [0, 4], color: "gray8" }}>
                {totalLabel}
              </Text>
              <div
                sx={{
                  color: "gray10",
                  fontSize: "18px",
                  fontWeight: 600,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  height: "36px",
                  borderRadius: "md",
                  mr: [0, 10],
                }}
              >
                <span
                  sx={{
                    maxWidth: "200px",
                    textOverflow: "ellipsis",
                    overflow: "hidden",
                    whiteSpace: "nowrap",
                  }}
                >
                  {formatPrice(total)}
                </span>
              </div>
            </Flex>
          </Box>
        </div>
      </Container>
    </Box>
  );
};

function calculateTotal(
  values: any,
  backupGBvalue: number,
  creditsPerMonth: number | null | undefined
) {
  const objectValues = Object.values(values) as any;
  const subtotal =
    objectValues.reduce((t: any, v: any) => t + v, 0) + backupGBvalue;
  if (!creditsPerMonth) return subtotal;
  return subtotal < creditsPerMonth ? creditsPerMonth : subtotal;
}

export default PriceCalculatorSection;
export { buttonWidth };
