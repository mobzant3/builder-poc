import "@/styles/globals.css";
import { Builder } from "@builder.io/react";
import type { AppProps } from "next/app";
import { ThemeProvider } from "theme-ui";
import PriceCalculatorSection from "../components/Calculator/Calculator";
import Container from "../components/Container";
import Hero from "../components/Hero";
import InlineStats from "../components/Inline Stats/InlineStats";
import Text from "../components/Text";
import TwoInlineCards from "../components/Two Inline Cards/TwoInlineCards";
import ZigZagNB from "../components/Zig Zag/ZigZagNB";
import theme from "../styles/theme";

Builder.registerComponent(Container, {
  name: "Container",
  inputs: [
    { name: "backgroundColor", type: "text" },
    { name: "imageUrl", type: "text" },
    { name: "maxWidth", type: "number" },
    { name: "height", type: "number" },
  ],
  canHaveChildren: true,
});

Builder.registerComponent(Text, {
  name: "CustomText",
  inputs: [
    { name: "text", type: "text" },
    { name: "asElement", type: "text" },
    { name: "variant", type: "text" },
  ],
});

Builder.registerComponent(Hero, {
  name: "Hero",
  inputs: [
    { name: "title", type: "text", defaultValue: "Fauna" },
    {
      name: "subtitle",
      type: "text",
      defaultValue:
        "Our distributed serverless database sets dev teams free to build faster, scale confidently, and soar above all operational obstacles.",
    },
    { name: "fontColor", type: "text", defaultValue: "white" },
    {
      name: "imageUrl",
      type: "text",
      defaultValue:
        "https://fauna.com/images/backgrounds/dark-light-curve@1x.png",
    },
    {
      name: "buttons",
      type: "list",
      subFields: [
        {
          name: "button",
          type: "object",
          subFields: [
            { name: "label", type: "text", defaultValue: "Button" },
            {
              name: "href",
              type: "text",
              defaultValue: "https://dashboard.fauna.com/accounts/register",
            },
          ],
        },
      ],
    },
  ],
});

Builder.registerComponent(ZigZagNB, {
  name: "ZigZagNB",
  inputs: [
    {
      name: "cards",
      type: "list",
      subFields: [
        {
          name: "card",
          type: "object",
          subFields: [
            { name: "title", type: "text" },
            { name: "subtitle", type: "text" },
            { name: "imageUrl", type: "text" },
          ],
        },
      ],
    },
  ],
});

Builder.registerComponent(InlineStats, {
  name: "InlineStats",
  inputs: [
    {
      name: "cards",
      type: "list",
      subFields: [
        {
          name: "card",
          type: "object",
          subFields: [
            {
              name: "title",
              type: "text",
            },
            {
              name: "subtitle",
              type: "text",
            },
          ],
        },
      ],
    },
  ],
});

Builder.registerComponent(PriceCalculatorSection, {
  name: "PriceCalculator",
  inputs: [
    {
      name: "blocks",
      type: "list",
      subFields: [
        { name: "name", type: "text" },
        { name: "description", type: "text" },
        { name: "creditsPerMonth", type: "number" },
        {
          name: "listItems",
          type: "list",
          subFields: [{ name: "title", type: "text" }],
        },
        {
          name: "usageRate",
          type: "object",
          subFields: [
            {
              name: "id",
              type: "text",
            },
            {
              name: "label",
              type: "text",
            },
            {
              name: "readOperations",
              type: "number",
            },
            {
              name: "writeOperations",
              type: "number",
            },
            {
              name: "transactionalComputeOperations",
              type: "number",
            },
            {
              name: "storage",
              type: "number",
            },
          ],
        },
        {
          name: "usageRatesCollection",
          type: "list",
          subFields: [
            {
              name: "id",
              type: "text",
            },
            {
              name: "label",
              type: "text",
            },
            {
              name: "readOperations",
              type: "number",
            },
            {
              name: "writeOperations",
              type: "number",
            },
            {
              name: "transactionalComputeOperations",
              type: "number",
            },
            {
              name: "storage",
              type: "number",
            },
          ],
        },
      ],
    },
  ],
});

Builder.registerComponent(TwoInlineCards, {
  name: "TwoInlineCards",
  inputs: [
    {
      name: "cards",
      type: "list",
      subFields: [
        {
          name: "card",
          type: "object",
          subFields: [
            { name: "title", type: "text" },
            { name: "subtitle", type: "text" },
            { name: "icon", type: "text" },
            { name: "additionalTitle", type: "text" },
            {
              name: "action",
              type: "object",
              subFields: [
                { name: "label", type: "text" },
                { name: "href", type: "text" },
              ],
            },
          ],
        },
      ],
    },
  ],
});

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={theme as any}>
      <Component {...pageProps} />
    </ThemeProvider>
  );
}
