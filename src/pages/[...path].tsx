import builder, { Builder, BuilderComponent } from "@builder.io/react";
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";

export async function getStaticProps({ params }: any) {
  const page = await builder
    .get("page", {
      apiKey: "72fd2c7b8b5d42b39d102460089f9251",
      url: `/${params.path}`,
    })
    .toPromise();
  return {
    props: {
      page,
    },
    revalidate: 1,
  };
}

export async function getStaticPaths() {
  const pages = await builder.getAll("page", {
    options: { noTargeting: true },
    apiKey: "72fd2c7b8b5d42b39d102460089f9251",
  });

  return {
    paths: [],
    fallback: true,
  };
}

export default function Path({ page }: any) {
  const router = useRouter();

  if (router.isFallback) {
    return <h1>Loading...</h1>;
  }

  const isLive = !Builder.isEditing && Builder.isPreviewing;
  if (!page && isLive) {
    return (
      <>
        <Head>
          <meta name="robots" content="noindex" />
        </Head>
        <h1>404</h1>
        {/* <DefaultErrorPage statusCode={404}></DefaultErrorPage> */}
      </>
    );
  }

  const { title, description, image } = page?.data || {};
  return (
    <BuilderComponent options={{ includeRefs: true }} apiKey="72fd2c7b8b5d42b39d102460089f9251" renderLink={Link as any} model="page" content={page} />
  );
}
