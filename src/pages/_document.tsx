import { Html, Head, Main, NextScript } from "next/document";
import FontAwesomeScript from "../lib/fontawesome";

export default function Document() {
  return (
    <Html lang="en">
      <Head />
      <body>
        <FontAwesomeScript />
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
